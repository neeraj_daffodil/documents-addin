﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Daffodil.SP.DocumentAddIn.Library
{
   public class Constants
    {
        public class Customers
        {
            public static string ListTitle = "Customers";

            public class Fields
            {
                public string ID = "fa564e0f-0c70-4ab9-b863-0177e6ddd247";                
            }
        }

        public class Projects
        {
            public static string ListTitle = "Projects";
            public class Fields
            {
                public static string ID= "A34F8BDA-CFB7-4026-BE78-537E6F174949";
            }
        }

        public class Contacts
        {
            public static string ListTitle = "Contacts";
            public class Fields
            {
                public static string ID = "A34F8BDA-CFB7-4026-BE78-537E6F174949";
            }
        }

        public class DocumentLibrary
        {
            public static string ListTitle = "Document Library";
            public class Fields
            {
                public static string CompanyNameId = "84553902-DC33-4142-90AA-1D903BAFDD09";
                public static string FirstNameId = "D68DEF69-73E0-4FBA-B511-EAF9643276E9";
                public static string ProjectId = "4EBB5520-5B33-4DA6-8C1F-2A40577A4268";
            }
        }
    }
}
