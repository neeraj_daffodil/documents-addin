using System;
using System.Runtime.InteropServices;
using System.Security.Permissions;
using Microsoft.SharePoint;
using Daffodil.SP.DocumentAddIn.Library;

namespace Daffodil.SP.DocumentAddIn.Features.DocumentAddinLists
{
    /// <summary>
    /// This class handles events raised during feature activation, deactivation, installation, uninstallation, and upgrade.
    /// </summary>
    /// <remarks>
    /// The GUID attached to this class may be used during packaging and should not be modified.
    /// </remarks>

    [Guid("83eaba2c-f2b0-4194-8f71-effc49ca23a2")]
    public class DocumentAddinListsEventReceiver : SPFeatureReceiver
    {
        // Uncomment the method below to handle the event raised after a feature has been activated.
        
        ///declaration area
        public override void FeatureActivated(SPFeatureReceiverProperties properties)
        {
            SPWeb spWeb = properties.Feature.Parent as SPWeb;
            SPList projectsList = spWeb.Lists.TryGetList("Projects");
            SPList contactList = spWeb.Lists.TryGetList("Contacts");
            SPList documentList = spWeb.Lists.TryGetList("Document Library");
            if (projectsList != null)
            {
                SPField companyField = projectsList.Fields.TryGetFieldByStaticName("documentAddinCustomerName");
                if (companyField != null)
                {
                    SPFieldLookup companyLookupField = companyField as SPFieldLookup;
                    companyLookupField.SchemaXml = companyLookupField.SchemaXml.Replace(Constants.Projects.Fields.ID, GetListID(spWeb, Constants.Customers.ListTitle));
                    companyLookupField.Update();
                }
            }

            if (contactList != null)
            {
                SPField companyField = contactList.Fields.TryGetFieldByStaticName("documentAddinCustomerName");
                if (companyField != null)
                {                    
                    SPFieldLookup companyLookupField = companyField as SPFieldLookup;
                    companyLookupField.SchemaXml = companyLookupField.SchemaXml.Replace(Constants.Contacts.Fields.ID, GetListID(spWeb, Constants.Customers.ListTitle));
                    companyLookupField.Update();
                }
            }

            if (documentList != null)
            {
                SPField companyField = documentList.Fields.TryGetFieldByStaticName("documentAddinCompany");
                if (companyField != null)
                {
                    SPFieldLookup companyLookupField = companyField as SPFieldLookup;
                    companyLookupField.SchemaXml = companyLookupField.SchemaXml.Replace(Constants.DocumentLibrary.Fields.CompanyNameId, GetListID(spWeb, Constants.Customers.ListTitle));
                    companyLookupField.Update();
                }
            }

            if(documentList!=null)
            {
                SPField projectField = documentList.Fields.TryGetFieldByStaticName("documentAddinProject");
                if (projectField != null)
                {
                    SPFieldLookup projectLookupField = projectField as SPFieldLookup;
                    projectLookupField.SchemaXml = projectLookupField.SchemaXml.Replace(Constants.DocumentLibrary.Fields.ProjectId,GetListID(spWeb,Constants.Projects.ListTitle));
                }
            }

         if (documentList != null)
            {
                SPField contactField = documentList.Fields.TryGetFieldByStaticName("documentAddinContact");
                if(contactField!=null){
                    SPFieldLookup contactLookupField = contactField as SPFieldLookup;
                    contactLookupField.SchemaXml = contactLookupField.SchemaXml.Replace(Constants.DocumentLibrary.Fields.FirstNameId,GetListID(spWeb,Constants.Contacts.ListTitle));
                }
            }
        }

        private string GetListID(SPWeb spWeb, string listName)
        {
            SPList list = spWeb.Lists.TryGetList(listName);
            if (list != null)
            {
                return Convert.ToString(list.ID);
            }
            return string.Empty;
        }


        // Uncomment the method below to handle the event raised before a feature is deactivated.

        //public override void FeatureDeactivating(SPFeatureReceiverProperties properties)
        //{
        //}


        // Uncomment the method below to handle the event raised after a feature has been installed.

        //public override void FeatureInstalled(SPFeatureReceiverProperties properties)
        //{
        //}


        // Uncomment the method below to handle the event raised before a feature is uninstalled.

        //public override void FeatureUninstalling(SPFeatureReceiverProperties properties)
        //{
        //}

        // Uncomment the method below to handle the event raised when a feature is upgrading.

        //public override void FeatureUpgrading(SPFeatureReceiverProperties properties, string upgradeActionName, System.Collections.Generic.IDictionary<string, string> parameters)
        //{
        //}
    }
}
