﻿<?xml version="1.0" encoding="utf-8"?>
<feature xmlns:dm0="http://schemas.microsoft.com/VisualStudio/2008/DslTools/Core" dslVersion="1.0.0.0" Id="8445bc4d-e75e-4f45-a767-6264a01b125e" description="Features for list definitions." featureId="8445bc4d-e75e-4f45-a767-6264a01b125e" imageUrl="" receiverAssembly="$SharePoint.Project.AssemblyFullName$" receiverClass="$SharePoint.Type.83eaba2c-f2b0-4194-8f71-effc49ca23a2.FullName$" solutionId="00000000-0000-0000-0000-000000000000" title="Document AddIn Lists Feature" version="" deploymentPath="$SharePoint.Project.FileNameWithoutExtension$_$SharePoint.Feature.FileNameWithoutExtension$" xmlns="http://schemas.microsoft.com/VisualStudio/2008/SharePointTools/FeatureModel">
  <projectItems>
    <projectItemReference itemId="290a16a3-a830-4248-9d94-489766a36c6c" />
    <projectItemReference itemId="f749f95f-493b-474c-b053-5571147be390" />
    <projectItemReference itemId="a5760916-54af-43a6-9423-31dd018e7455" />
    <projectItemReference itemId="40b93eae-33ae-403b-86e4-7e33e24626a0" />
    <projectItemReference itemId="a2ae1aab-4142-45bd-81f6-b8a1bc2e314a" />
    <projectItemReference itemId="3d48728b-3f39-49a1-8d70-7e5cbddd99d6" />
    <projectItemReference itemId="79afe43e-6a76-495b-b55f-793862722bf8" />
    <projectItemReference itemId="d24658df-ec01-4371-90a1-0ad4e7d39c5c" />
  </projectItems>
</feature>